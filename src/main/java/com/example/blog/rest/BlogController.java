package com.example.blog.rest;

import com.example.blog.dto.BlogDto;
import com.example.blog.dto.CreateBlogDto;
import com.example.blog.service.BlogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/blogs")
public class BlogController {

    private final BlogService blogService;


    @GetMapping("/page")
    public ResponseEntity<Page<BlogDto>> getBlogsAndCommentsPageable(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {

        log.info("Get blogs and comments pageable");

        Page<BlogDto> blogDtos = blogService.getBlogsAndCommentsPageable(page, size);

        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Page-Number", String.valueOf(blogDtos.getNumber()));
        headers.add("X-Page-Size", String.valueOf(blogDtos.getSize()));

        return new ResponseEntity(blogDtos, headers, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<BlogDto>> getBlogsAndComments() {
        log.info("Get blogs and comments");

        return new ResponseEntity(blogService.getBlogsAndComments(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> saveBlog(@RequestBody CreateBlogDto createBlogDto) {
        log.info("Received Blog DTO to save: {}", createBlogDto);

        try {
            String message = blogService.saveBlog(createBlogDto);
            return new ResponseEntity(message, HttpStatus.OK);

        } catch (RuntimeException re) {
            return new ResponseEntity("Unsuccessfully saved blog", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
