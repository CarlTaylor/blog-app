package com.example.blog.rest;


import com.example.blog.dto.CreateCommentDto;
import com.example.blog.service.CommentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/comments")
public class CommentController {

    private final CommentService commentService;

    @PostMapping
    public ResponseEntity<String> saveComment(@RequestBody CreateCommentDto createCommentDto) {
        log.info("Received comment DTO to save: {}", createCommentDto);

        try {
            String message = commentService.saveComment(createCommentDto);
            return new ResponseEntity(message, HttpStatus.OK);

        } catch (RuntimeException re) {
            return new ResponseEntity("Unsuccessfully saved blog", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
