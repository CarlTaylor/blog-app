package com.example.blog.service;

import com.example.blog.dto.BlogDto;
import com.example.blog.dto.CommentDto;
import com.example.blog.dto.CreateBlogDto;
import com.example.blog.mapper.BlogMapper;
import com.example.blog.model.Blog;
import com.example.blog.model.Comment;
import com.example.blog.repository.BlogRepository;
import com.example.blog.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class BlogService {

    private final BlogRepository blogRepository;
    private final CommentRepository commentRepository;

    private final BlogMapper mapper = Mappers.getMapper(BlogMapper.class);

    public String saveBlog(CreateBlogDto createBlogDto) {
        Blog createBlog = mapper.map(createBlogDto);

        try {
            Blog savedBlog = blogRepository.save(createBlog);

            return String.format("%s", savedBlog.getId());
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public List<BlogDto> getBlogsAndComments() {
        List<Blog> blogList = blogRepository.findAll();

        List<BlogDto> blogDtoList = blogList
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());

        if (!blogDtoList.isEmpty()) {
            blogDtoList.stream().forEach(blogDto -> {
                List<Comment> commentList = commentRepository.findAllByBlogId(blogDto.getId());

                if (!commentList.isEmpty()) {
                    List<CommentDto> commentDtoList =
                            commentList.stream()
                                    .map(mapper::map)
                                    .collect(Collectors.toList());

                    blogDto.setCommentDtos(commentDtoList);
                }
            });
        }

        return blogDtoList;
    }

    public Page<BlogDto> getBlogsAndCommentsPageable(int page, int size) {
        Pageable pageRequest = PageRequest.of(page, size);

        List<BlogDto> blogDtos = blogRepository.findAll(pageRequest)
                .stream()
                .map(mapper::map)
                .toList();

        blogDtos.stream().forEach(blogDto -> blogDto.setCommentDtos(commentRepository.findAllByBlogId(blogDto.getId())
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList())));

        return new PageImpl<>(blogDtos, pageRequest, blogDtos.size());
    }
}
