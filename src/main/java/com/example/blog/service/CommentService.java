package com.example.blog.service;

import com.example.blog.dto.CreateCommentDto;
import com.example.blog.mapper.BlogMapper;
import com.example.blog.model.Comment;
import com.example.blog.repository.CommentRepository;
import lombok.RequiredArgsConstructor;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class CommentService {

    private final CommentRepository commentRepository;

    private final BlogMapper mapper = Mappers.getMapper(BlogMapper.class);

    public String saveComment(CreateCommentDto createCommentDto) {
        Comment createComment = mapper.map(createCommentDto);

        try {
            Comment savedComment = commentRepository.save(createComment);

            return String.format("%s", savedComment.getId());
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
