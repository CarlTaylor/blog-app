package com.example.blog.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
public class BlogDto {
    private UUID id;
    private String title;
    private String text;
    private List<CommentDto> commentDtos;
}
