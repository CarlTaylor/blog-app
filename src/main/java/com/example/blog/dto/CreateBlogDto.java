package com.example.blog.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBlogDto {
    private String title;
    private String text;
}
