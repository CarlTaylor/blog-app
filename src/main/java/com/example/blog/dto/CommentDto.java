package com.example.blog.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Builder
public class CommentDto {
    private UUID id;
    private String text;
    private Instant createdAt;
}
