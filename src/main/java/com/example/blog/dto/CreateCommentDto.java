package com.example.blog.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class CreateCommentDto {
    private UUID blogId;
    private String text;
}
