package com.example.blog.mapper;

import com.example.blog.dto.BlogDto;
import com.example.blog.dto.CommentDto;
import com.example.blog.dto.CreateBlogDto;
import com.example.blog.dto.CreateCommentDto;
import com.example.blog.model.Blog;
import com.example.blog.model.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface BlogMapper {
    Blog map(CreateBlogDto createBlog);

    @Mapping(target = "blog.id", source = "blogId")
    Comment map(CreateCommentDto createComment);

    BlogDto map(Blog blog);

    CommentDto map(Comment comment);
}
