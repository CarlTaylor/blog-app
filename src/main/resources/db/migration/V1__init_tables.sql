CREATE TABLE public.blogs
(
    id         uuid NOT NULL,
    created_at timestamptz(6) NULL,
    "text"     varchar(255) NULL,
    title      varchar(255) NULL,
    CONSTRAINT blogs_pkey PRIMARY KEY (id)
);

CREATE TABLE public."comments"
(
    id         uuid NOT NULL,
    created_at timestamptz(6) NULL,
    "text"     varchar(255) NULL,
    blog_id    uuid NOT NULL,
    CONSTRAINT comments_pkey PRIMARY KEY (id)
);

ALTER TABLE public."comments"
    ADD CONSTRAINT fk_blog_comment FOREIGN KEY (blog_id) REFERENCES public.blogs (id);