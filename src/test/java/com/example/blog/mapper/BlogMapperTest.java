package com.example.blog.mapper;

import com.example.blog.dto.CreateCommentDto;
import com.example.blog.model.Comment;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class BlogMapperTest {

    private final BlogMapper mapper = Mappers.getMapper(BlogMapper.class);

    @Test
    void map() {
        CreateCommentDto createCommentDto = new CreateCommentDto();
        createCommentDto.setBlogId(UUID.randomUUID());
        createCommentDto.setText("Comment text");

        Comment comment = mapper.map(createCommentDto);

        assertEquals(createCommentDto.getBlogId(), comment.getBlog().getId());
        assertEquals(createCommentDto.getText(), comment.getText());
    }
}