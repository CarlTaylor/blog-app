FROM gradle:7.5-jdk17-alpine AS BUILD
WORKDIR /usr/app/
COPY . .
RUN gradle clean build -x test

FROM openjdk:17
ENV JAR_NAME=blog-0.0.1-SNAPSHOT.jar
ENV APP_HOME=/usr/app
WORKDIR $APP_HOME
COPY --from=BUILD $APP_HOME .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/usr/app/build/libs/blog-0.0.1-SNAPSHOT.jar"]
